package main

import "fmt"

func swap(x, y string) (string, string) {
	return y, x

}
func true_swap(x, y *int) {

	var temp int
	temp = *x
	*x = *y
	*y = temp

}

func main() {
	a, b := swap("a", "b")
	fmt.Println(a, b)
	c, d := 1, 2
	true_swap(&c, &d)
	fmt.Println(c, d)
}
