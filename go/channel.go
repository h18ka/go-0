package main

import (
	"fmt"
    "time"
)

func sum(a []int, c chan int, flag int) {

	total := 0
	for _, v := range a {

		total += v
	}
    if flag==1 {

        time.Sleep(900000000)
        //fmt.Println("i am here")
    }

	c <- total
    //fmt.Println(flag, " goes in..")

}

func main() {
	a := []int{1, 2, 3, 4, 5, 6, 7, 8}
	c := make(chan int)

	go sum(a[:len(a)/2], c, 0)
	go sum(a[len(a)/2:], c, 1)
	//x, y := <-c, <-c
    x := <-c
    //fmt.Println(x)
    y := <-c

	fmt.Println(x, y, x+y)
	// 26, 10, 36

}
