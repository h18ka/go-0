/*
Go 语言中同时有函数和方法。一个方法就是一个包含了接受者的函数，
接受者可以是命名类型或者结构体类型的一个值或者是一个指针。
所有给定类型的方法属于该类型的方法集。语法格式如下：

func (variable_name variable_data_type) function_name() [return_type]{
	    //函数体
   }
*/
package main

import (
	"fmt"
)

type Computer struct {
	main_board string
	cpu        string
	ram        int
	ssd        int
	monitor    float64
}

func main() {
	//var kk Computer
	//kk.main_board = "b80"
	//kk.cpu = "g230"
	//kk.ram = 8
	//kk.ssd = 256
	//kk.monitor = 23.8
	kk := Computer{"b812", "g230", 8, 256, 23.8}
	fmt.Println("You computer storage part cost: ", kk.storage_cost())

}

func (k Computer) storage_cost() float64 {

	//	return k.ram*400.1 + 600.1
	return 400.0 * float64(k.ram)
}
