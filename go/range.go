package main

import "fmt"

func main() {
	nums := []int{3, 4, 5}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println(sum)

	for i, num := range nums {
		fmt.Println(i, " -> ", num)
	}

	for i, c := range "happy" {
		fmt.Println(i, c)
	}

	kvs := map[string]string{"chrome": "browser", "xshell": "ssh", "qq": "im"}
	for k, v := range kvs {
		fmt.Println(k, v)
	}
}
