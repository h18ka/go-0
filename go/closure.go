package main

import "fmt"

func getRoot() func() int {
	i := 0
	return func() int {
		i += 1
		return i
	}
}

func main() {

	a := getRoot()
	// a是函数

	fmt.Println(a)
	fmt.Println(a())
	fmt.Println(a())
	fmt.Println(a())

	b := getRoot()

	fmt.Println(b())
	fmt.Println(b())
	fmt.Println(b())
}
