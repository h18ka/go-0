package main

import "fmt"

func main() {
	const (
		a = iota
		b
		c
		d = "ab"
		e
		f = 100
		g
		h
		i = iota
		j
		k
	)

	fmt.Println(a, b, c, d, e, f, g, h, i, j, k)
}
