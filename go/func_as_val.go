package main

import (
	"fmt"
	"math"
)

func main() {

	getRoot := func(x float64) float64 {
		return math.Sqrt(x)
	}

	fmt.Println(getRoot(16))

}
