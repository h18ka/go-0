package main

import "fmt"

func main() {

	var n = [5]int{23, 6, 12, 7, 30}
	var i int

	for i = 0; i < 5; i++ {

		fmt.Printf("Element[%d] = %d\n", i, n[i])

	}

}
